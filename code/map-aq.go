package main

import (
	"sync"
)

type coinTracker struct {
	// lastRates map[string]ResolvedRate
	sync.Map
}

type ResolvedRate struct {
	X, Y float64
	Err  error
}

func (t *coinTracker) SetLastRate(quotationKey string, newRate ResolvedRate) {
	t.Store(quotationKey, newRate) // write
}

func (t *coinTracker) GetLastRate(quotationKey string) ResolvedRate {
	oldRate, _ := t.Load(quotationKey) // read
	return oldRate.(ResolvedRate)
}

type coinTracker3 struct {
	lastRates map[string]ResolvedRate
	sync.Mutex
}

type ResolvedRate3 struct {
	X, Y float64
	Err  error
}

func (t *coinTracker3) SetLastRate3(quotationKey string, newRate ResolvedRate) {
	t.Lock()
	defer t.Unlock()
	t.lastRates[quotationKey] = newRate // write
}

func (t *coinTracker3) GetLastRate3(quotationKey string) ResolvedRate {
	t.Lock()
	defer t.Unlock()
	oldRate := t.lastRates[quotationKey] // read
	return oldRate
}

type coinTracker2 struct {
	lastRates map[string]ResolvedRate
	sync.RWMutex
}

type ResolvedRate2 struct {
	X, Y float64
	Err  error
}

func (t *coinTracker2) SetLastRate2(quotationKey string, newRate ResolvedRate) {
	t.Lock()
	defer t.Unlock()
	t.lastRates[quotationKey] = newRate // write
}

func (t *coinTracker2) GetLastRate2(quotationKey string) ResolvedRate {
	t.RLock()
	t.RUnlock()
	oldRate := t.lastRates[quotationKey] // read
	return oldRate
}


