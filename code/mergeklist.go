package main

import (
	"fmt"
)


type ListNode struct {
	Value int
	Next  *ListNode
}

func main() {
	//var lists = make([]*ListNode, 2)
	//fmt.Println(mergeKLists(lists))
	var list1 = ListNode{Value: 1, Next: nil}
	fmt.Println("list %v", list1)
	//lists = append(lists, &list1)
	//var list2 = ListNode{Value: 3, Next: nil}
	//lists = append(lists, &list2)
	//list := mergeKLists(lists)
	//fmt.Println(list.Value)
	//for i := 0; i < len(lists); i++  {
	//	fmt.Println(lists[i].Value)
	//}
	return
}

func mergeKLists(lists []*ListNode) *ListNode {
	if len(lists) == 0 {
		return nil
	}
	begin, end := 0, len(lists) - 1
	for begin < end {
		mid := (begin + end - 1) / 2
		for i := 0; i <= mid; i++ {
			lists[i] = mergeTwoLists(lists[i], lists[end - i])
		}
		end = (begin + end) / 2
	}
	return lists[0]
}

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	if l1.Value < l2.Value {
		l1.Next = mergeTwoLists(l1.Next, l2)
		return l1
	} else {
		l2.Next = mergeTwoLists(l1, l2.Next)
		return l2
	}
}
