package main

import "fmt"

// Leetcode 求众数

func main()  {
	var arr = []int{3,2,3}
	fmt.Println(majorityElement(arr))
	var arr2 = []int{2,2,1,1,1,2,2}
	fmt.Println(majorityElement(arr2))
	return
}

func majorityElement(nums []int) int {
	m := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		num := nums[i]
		m[num] = m[num] + 1
		if m[num] > len(nums)/2 {
			return num
		}
	}
	return 0
}