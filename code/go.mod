module go-code/code

go 1.12

require (
	github.com/Shopify/sarama v1.24.1
	github.com/aws/aws-sdk-go v1.25.13
	github.com/hashicorp/go-retryablehttp v0.6.3
	github.com/klauspost/cpuid v1.2.1 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/net v0.0.0-20191014212845-da9a3fd4c582 // indirect
)
