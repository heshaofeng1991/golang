package main

//import (
//	"fmt"
//	"log"
//	"math"
//	"net/http"
//	"os"
//	"strconv"
//)
//
//func main()  {
//	fmt.Println(strconv.FormatFloat(math.Abs(-20.11), 'f', -1, 64))
//	if (true) {
//		defer fmt.Printf("1")
//	} else {
//		defer fmt.Printf("2")
//	}
//	fmt.Println("3")
//	fmt.Println("33")
//}

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

type Message struct {
	WalletType string `json:"wallet_type"`
}

func main() {
	msg := Message{}
	msg.WalletType = "crypto"
	b, _ := json.Marshal(msg)
	req, err := http.NewRequest("GET", "http://api.themoviedb.org/3/tv/popular?text=poll", bytes.NewBuffer(b))
	if err != nil {
		log.Print(err)
		os.Exit(1)
	}

	q := req.URL.Query().Get("text")
	rr, _ := ioutil.ReadAll(req.Body)
	result := json.Unmarshal(rr, &msg)

	fmt.Println(q)
	fmt.Println(result)
	fmt.Println(msg.WalletType)
	fmt.Println(string(rr))
	// Output:
	// http://api.themoviedb.org/3/tv/popular?another_thing=foo+%26+bar&api_key=key_from_environment_or_flag
}
