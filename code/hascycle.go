package main

import (
	"fmt"
	"math"
	"time"
)

// LeeCode 环形链表

//type ListNode struct {
//	Value  int
//	Next *ListNode
//}

func main()  {
	var i int
	i = 7
	j := time.Duration(i)
	fmt.Println(j * 24 * time.Hour)
	diff := 100.00
	percentage := diff / 1000
	fmt.Println("111", percentage)
	fmt.Println(math.Abs(percentage*100))
	percentage = math.Round(percentage*10) / 10
	fmt.Println("222", percentage)
	//queryType := "crypto?"
	//queryType = strings.Split(queryType, "?")[0]
	//fmt.Println("11111", queryType)
	//tp.Infof("queryType %v", queryType)
	//var listNode ListNode
	//fmt.Println(hasCycle(&listNode)) // false 无环， true 有环
	return
}

//func hasCycle(head *ListNode) bool {
//	if head == nil {
//		return false
//	}
//	prv, next := head.Next, head.Next
//	for prv != nil && prv.Next != nil && next != nil && next.Next != nil {
//		prv = prv.Next
//		next = next.Next.Next
//		if prv == next {
//			return true
//		}
//	}
//	return false
//}
