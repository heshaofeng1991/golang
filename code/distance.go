package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

//type ProfitCoin struct {
//	Symbol      string  `json:"id,omitempty"`
//	Amount      *Amount  `json:"amount,omitempty"`
//	PriceNative *Amount  `json:"price_native,omitempty"`
//	PriceCrypto *Amount  `json:"price_crypto,omitempty"`
//	Percentage  string  `json:"percentage,omitempty"`
//	PercentChangeNative  string  `json:"percent_change_native,omitempty"`
//	PercentChangeCrypto  string  `json:"percent_change_crypto,omitempty"`
//}

//type coinOrder []*ProfitCoin
//func (a coinOrder) Len() int           { return len(a) }
//func (a coinOrder) Less(i, j int) bool { return StringToFloat64(a[i].PriceNative.Amount) > StringToFloat64(a[j].PriceNative.Amount) || a[i].Symbol < a[j].Symbol }
//func (a coinOrder) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

//{
//"id": "BNB",
//"amount": {
//"currency": "BNB",
//"amount": "0.0017266419832"
//},
//"price_native": {
//"currency": "USD",
//"amount": "0.02"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "0.000002063073255527792"
//},
//"percentage": "0.00000000864638914208282"
//}, {
//"id": "BTC",
//"amount": {
//"currency": "BTC",
//"amount": "3.89776637"
//},
//"price_native": {
//"currency": "USD",
//"amount": "27803.16"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "2.8679977907580043"
//},
//"percentage": "0.012019847036979567"
//}, {
//"id": "EOS",
//"amount": {
//"currency": "EOS",
//"amount": "5000.0"
//},
//"price_native": {
//"currency": "USD",
//"amount": "12172.5"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "1.2556379601456022"
//},
//"percentage": "0.005262408591600156"
//}, {
//"id": "ETH",
//"amount": {
//"currency": "ETH",
//"amount": "5.385567284078850919"
//},
//"price_native": {
//"currency": "USD",
//"amount": "682.89"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "0.0704426047733687"
//},
//"percentage": "0.0002952266340618468"
//}, {
//"id": "CRO",
//"amount": {
//"currency": "CRO",
//"amount": "8563.8538971"
//},
//"price_native": {
//"currency": "USD",
//"amount": "284.34"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "0.029330712473838616"
//},
//"percentage": "0.00012292571443299144"
//}, {
//"id": "LTC",
//"amount": {
//"currency": "LTC",
//"amount": "4.7698702"
//},
//"price_native": {
//"currency": "USD",
//"amount": "189.08"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "0.019504294557759747"
//},
//"percentage": "0.00008174296294925098"
//}, {
//"id": "MCO",
//"amount": {
//"currency": "MCO",
//"amount": "60478185.03553102"
//},
//"price_native": {
//"currency": "USD",
//"amount": "231268579.58"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "23856.201068769944"
//},
//"percentage": "99.98190676927142"
//}, {
//"id": "XRP",
//"amount": {
//"currency": "XRP",
//"amount": "19.6"
//},
//"price_native": {
//"currency": "USD",
//"amount": "3.66"
//},
//"price_crypto": {
//"currency": "BTC",
//"amount": "0.0003775424057615859"
//},
//"percentage": "0.0000015822892130011558"
//}



func main() {
	client := http.Client{
		Timeout: time.Duration(3 * time.Second),
	}

	req, err := http.NewRequest(http.MethodGet, "http://www.baidu.com", nil)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	if resp.StatusCode != 200 {
		fmt.Println(string(body))
	}

	//people := []struct {
	//	ID string
	//	Percentage  int
	//}{
	//	{"Crypto Wallet", 1},
	//	{"Earn", 1},
	//	{"Credit", 1},
	//}
	//
	//sort.SliceStable(people, func(i, j int) bool {
	//	if people[i].Percentage == people[j].Percentage {
	//		return people[i].ID < people[j].ID
	//	}
	//	return people[i].Percentage >= people[j].Percentage
	//}) // 按年龄降序排序
	//fmt.Println("Sort by age1:", people)

	//distance, _ := Distance("aaaddd", "asfddfdsfafasg")
	//fmt.Println(distance)
}

func Distance(a, b string) (int, error) {
	var distance int
	if a == "" && b == "" {
		return 0, nil
	} else if a == "" && b != "" {
		return strings.Count(b, "") - 1, nil
	} else if a != "" && b == "" {
		return strings.Count(a, "") - 1, nil
	} else {
		// compare a difference b string
		/*
		  7         l1:=len([]rune(str))
		  6         l2:=bytes.Count([]byte(str),nil)-1)
		  5         l3:=strings.Count(str,"")-1
		  4         l4:=utf8.RuneCountInString(str)
		  3     */
		originStr := []rune(a)
		destStr := []rune(b)
		lenOriginStr := len(originStr)
		lenDestStr := len(destStr)
		if lenOriginStr > lenDestStr {
			distance += lenOriginStr - lenDestStr
			for i := 0; i < lenDestStr; i++ {
				if originStr[i] != destStr[i] {
					distance++
				}
			}
			return distance, nil
		} else if lenOriginStr == lenDestStr {
			for i := 0; i < lenDestStr; i++ {
				if originStr[i] != destStr[i] {
					distance++
				}
			}
			return distance, nil

		} else {
			distance += lenDestStr - lenOriginStr
			for i := 0; i < lenOriginStr; i++ {
				if originStr[i] != destStr[i] {
					distance++
				}
			}
			return distance, nil

		}
	}
}
