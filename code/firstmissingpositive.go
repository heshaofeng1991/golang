package main

import (
	"fmt"
)

// LeeCode 缺失的第一个正数
// 缺失的第一个正数不会大于 n+1；首先将小于 1 的值置为 n+1，然后数组的值作为下标，把相对应的位置 nums[v-1] 变为负数，最后遍历数组判断是否有正数

func main() {
	var arr = []int{}
	fmt.Println(firstMissingPositive(arr))
	return
}

func firstMissingPositive(nums []int) int {
	n := len(nums)
	for i, v := range nums {
		if v < 1 {
			nums[i] = n+1
		}
	}
	for _, v := range nums {
		v1 := abs(v)
		if v1 > 0 && v1 <= n {
			nums[v1-1] = -abs(nums[v1-1])
		}
	}
	for i, v := range nums {
		if v > 0 {
			return i+1
		}
	}
	return n+1
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}
