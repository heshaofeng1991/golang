package main

import (
	"fmt"
	"runtime"
)

// 随机事件 有可能输出，也有可能Panic
func main() {
	runtime.GOMAXPROCS(1)
	intChan := make(chan int, 1)
	stringChan := make(chan string, 1)
	intChan <- 1
	stringChan <- "hello"
	select {
	case value := <-intChan:
		fmt.Println(value)
	case value := <-stringChan:
		panic(value)
	}
}

/*
单个chan如果无缓冲时，将会阻塞。但结合 select可以在多个chan间等待执行。有三点原则：
1. select 中只要有一个case能return，则立刻执行。
2. 当如果同一时间有多个case均能return，则伪随机方式抽取任意一个执行。
3. 如果没有一个case能return，则可以执行”default”块。
*/