package main

import (
	"encoding/csv"
	"os"
	"time"
)

func main() {
	createCSV("test1.csv")
}

func createCSV(path string) error {
	fileName, err := os.Create(path)
	if err != nil {
		return err
	}

	title := []string{"Timestamp", "Transaction Description", "Currency", "Amount", "To Currency", "To Amount", "Native Currency", "Native Amount", "Native Amount (in USD)"}
	writer := csv.NewWriter(fileName)
	err = writer.Write(title)
	if err != nil {
		return err
	}
	writer.Flush() // Writes the buffered data to the writer
	err = writer.Error() // Checks if any error occurred while writing
	if err != nil {
		return err
	}

	record := []string{time.Now().Format("2006-01-02 15:04:05"), "tx record", "EUR", "100", "EUR", "100", "USD", "100", "113.7"}
	err = writer.Write(record)
	if err != nil {
		return err
	}
	writer.Flush() // Writes the buffered data to the writer
	err = writer.Error() // Checks if any error occurred while writing
	if err != nil {
		return err
	}

	err = fileName.Close()
	if err != nil {
		return err
	}
	return nil
}
