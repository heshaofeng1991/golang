package main

// Leetcode 三数之和

import (
	"fmt"
	"sort"
)

func main() {
	var arr = []int{-1, 0, 1, 2, -1, -4}
	r := threeSum2(arr)
	for i := 0; i < len(r); i++ {
		fmt.Println(r[i])
		for j := 0; j < i; j++ {
			fmt.Println(r[i][j])
		}
	}
}

func threeSum2(nums []int) [][]int {
	//先对数组排序
	sort.Ints(nums)
	result := [][]int{}
	for i := 0; i < len(nums)-1; i++ {
		//if i > 0 && nums[i] == nums[i-1] {
		//	continue
		//}
		j := i + 1
		z := len(nums) - 1
		for z > j {
			a := nums[i]
			b := nums[j]
			c := nums[z]
			if a+b+c > 0 {
				z--
			} else if a+b+c < 0 {
				j++
			} else {
				item := []int{a, b, c}
				result = append(result, item)
				for j < z && nums[j] == nums[j+1] {
					j++
				}
				for j < z && nums[z] == nums[z-1] {
					z--
				}
				j++
				z--
			}
		}
	}
	return result
}
