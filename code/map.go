package main

import (
	"encoding/json"
	"fmt"
	"strconv"
)

type student struct {
	Name string
	Age  int
}

func pase_student() map[string]*student {
	m := make(map[string]*student)
	stus := []student{
		{Name: "zhou", Age: 24},
		{Name: "li", Age: 23},
		{Name: "wang", Age: 22},
	}
	/*因为for遍历时，变量stu指针不变，每次遍历仅进行struct值拷贝，故m[stu.Name]=&stu实际上一致指向同一个指针，最终该指针的值为遍历的最后一个struct的值拷贝。*/
	//for _, stu := range stus {
	//	m[stu.Name] = &stu
	for i, _ := range stus {
		m[stus[i].Name] = &stus[i]
	}
	return m
}

const (
	InMax = 1999
	InMin = 1000
	OutMax = 2999
	OutMin = 2000
)

const (
	InFlow = "IN"
	OutFlow = "OUT"
)

// inflow
const (
	VibanPurchase              = "viban_purchase"
	AdminWalletCredited        = "admin_wallet_credited"
	CryptoDeposit              = "crypto_deposit"
	CryptoPurchase             = "crypto_purchase"
	CryptoExchangeIn           = "crypto_exchange"
	CryptoPaymentIN            = "crypto_payment"
	CryptoPaymentRefund        = "crypto_payment_refund"
	InvestWithdrawal           = "invest_withdrawal"
	ReferralCommission         = "referral_commission"
	ReferralGift               = "referral_gift"
	ReferralCryptoCashback     = "referral_crypto_cashback"
	ReferralCardCashback       = "referral_card_cashback"
	ReferralBonus              = "referral_bonus"
	CampaignReward             = "campaign_reward"
	VanPurchase                = "van_purchase"
	AirdropLocked              = "airdrop_locked"
	Reimbursement              = "reimbursement"
	CouncilNodeInterestCreated = "council_node_interest_created"
	CryptoEarnInterestPaid     = "crypto_earn_interest_paid"
	CryptoCreditLoanCredited   = "crypto_credit_loan_credited"
	GiftCardReward             = "gift_card_reward"
	TransferCashback           = "transfer_cashback"
	McoStakeReward             = "mco_stake_reward"
	PayCheckoutReward          = "pay_checkout_reward"
	StakingReward              = "staking_reward"
)

// outflow
const (
	AdminWalletDebited             = "admin_wallet_debited"
	CryptoWithdrawalW              = "crypto_withdrawal"
	CryptoExchangeOut              = "crypto_exchange"
	CryptoTransfer                 = "crypto_transfer"
	CryptoPaymentOut               = "crypto_payment"
	InvestDeposit                  = "invest_deposit"
	CardTopUp                      = "card_top_up"
	CardCashbackReverted           = "card_cashback_reverted"
	CryptoVibanExchange            = "crypto_viban_exchange"
	ReimbursementReverted          = "reimbursement_reverted"
	CryptoCreditRepaymentCreated   = "crypto_credit_repayment_created"
	CryptoCreditLiquidationCreated = "crypto_credit_liquidation_created"
)

// define map store all kind type
var KindValue map[string]int

func InitMap() map[string]int{
	KindValue := make(map[string]int, 40)
	// inflow [1000-2000)
	KindValue[AdminWalletCredited] = 1000
	KindValue[CryptoDeposit] = 1001
	KindValue[CryptoPurchase] = 1002
	KindValue[CryptoExchangeIn] = 1003
	KindValue[CryptoPaymentIN] = 1004
	KindValue[CryptoPaymentRefund] = 1005
	KindValue[InvestWithdrawal] = 1006
	KindValue[ReferralCommission] = 1007
	KindValue[ReferralGift] = 1008
	KindValue[ReferralCryptoCashback] = 1009
	KindValue[ReferralCardCashback] = 1010
	KindValue[ReferralBonus] = 1011
	KindValue[CampaignReward] = 1012
	KindValue[VanPurchase] = 1013
	KindValue[AirdropLocked] = 1014
	KindValue[Reimbursement] = 1015
	KindValue[CouncilNodeInterestCreated] = 1016
	KindValue[CryptoEarnInterestPaid] = 1017
	KindValue[CryptoCreditLoanCredited] = 1018
	KindValue[GiftCardReward] = 1019
	KindValue[TransferCashback] = 1020
	KindValue[McoStakeReward] = 1021
	KindValue[PayCheckoutReward] = 1022
	KindValue[StakingReward] = 1023

	// outflow [2000-3000)
	KindValue[AdminWalletDebited] = 2000
	KindValue[CryptoWithdrawalW] = 2001
	KindValue[CryptoExchangeOut] = 2002
	KindValue[CryptoTransfer] = 2003
	KindValue[CryptoPaymentOut] = 2004
	KindValue[InvestDeposit] = 2005
	KindValue[CardTopUp] = 2006
	KindValue[CardCashbackReverted] = 2007
	KindValue[CryptoVibanExchange] = 2008
	KindValue[ReimbursementReverted] = 2009
	KindValue[CryptoCreditRepaymentCreated] = 2010
	KindValue[CryptoCreditLiquidationCreated] = 2011

	// other need deal kind [3000,4000)
	KindValue[VibanPurchase] = 3000

	for _, value := range KindValue {
		fmt.Println("msg", "InitMap", "map value is ", value)
	}

	return KindValue
}

type AccountParse struct {
	Account AccountShow `json:"account,omitempty"`
	OK bool `json:"ok"`
}

type AccountShow struct {
	Id int64 `json:"id,omitempty"`
	NativeCurrency string `json:"native_currency,omitempty"`
	Balance Amount `json:"balance,omitempty"`
	NativeBalance Amount `json:"native_balance,omitempty"`
	Wallets []UserWallets `json:"wallets,omitempty"`
}

type UserWallets struct {
	Currency string `json:"currency,omitempty"`
	Balance Amount `json:"balance,omitempty"`
	NativeBalance Amount `json:"native_balance,omitempty"`
	PercentageChange string `json:"percentage_change,omitempty"`
}

type Amount struct {
	Currency  string  `json:"currency"`
	Amount  string  `json:"amount"`
}

func StringToFloat64(amount string) float64 {
	f, _ := strconv.ParseFloat(amount, 64)
	return f
}

type CreditAccountParse struct {
	CryptoCreditAccount CreditAccountShow `json:"crypto_credit_account,omitempty"`
	OK bool `json:"ok"`
}

type Programs struct {
	CollateralAmount Amount `json:"collateral_amount,omitempty"`
	CollateralNativeAmount Amount `json:"collateral_native_amount,omitempty"`
}

type CreditAccountShow struct {
	OutstandingBalance Amount `json:"outstanding_balance,omitempty"`
	Programs []Programs `json:"programs,omitempty"`
}


func main() {

	str := `{
  "crypto_credit_account": {
    "outstanding_balance": {
      "currency": "TUSD",
      "amount": "136.45"
    },
    "percentage_change": "1.26",
    "is_virgin": false,
    "recently_liquidated": false,
    "program_creation_paused": false,
    "programs": [
      {
        "id": "563b8b39-3174-4778-bb21-3faddda57696",
        "collateral_amount": {
          "currency": "MCO",
          "amount": "100.0"
        },
        "collateral_native_amount": {
          "currency": "USD",
          "amount": "335.76"
        },
        "loan_amount": {
          "currency": "TUSD",
          "amount": "136.45"
        },
        "collateral_health": "good",
        "collateral_health_zone": "A",
        "collateral_health_levels": [
          {
            "zone": "A",
            "above_ltv": "0.0",
            "below_ltv": "0.6",
            "collateral_health": "good"
          },
          {
            "zone": "B",
            "above_ltv": "0.6",
            "below_ltv": "0.7",
            "collateral_health": "fair"
          },
          {
            "zone": "C",
            "above_ltv": "0.7",
            "below_ltv": "0.8",
            "collateral_health": "poor"
          },
          {
            "zone": "D",
            "above_ltv": "0.8",
            "below_ltv": "0.85",
            "collateral_health": "critical"
          },
          {
            "zone": "E",
            "above_ltv": "0.85",
            "below_ltv": null,
            "collateral_health": "certified"
          }
        ],
        "collateral_health_calculating": false,
        "current_apr": "0.08",
        "current_daily_fee": {
          "currency": "TUSD",
          "amount": "0.03"
        },
        "rebalance": {
          "collateral_amount": {
            "currency": "MCO",
            "amount": "0.0"
          },
          "loan_amount": {
            "currency": "TUSD",
            "amount": "0.0"
          }
        },
        "term": {
          "id": "2118bb10-21f3-48c2-a408-a1bcbd4ef1f1",
          "collateral_currency": "MCO",
          "ltv": "0.4",
          "loan_choices": [
            {
              "currency": "TUSD",
              "max_fraction": 2,
              "min_amount": {
                "currency": "TUSD",
                "amount": "100.0"
              },
              "max_amount": {
                "currency": "TUSD",
                "amount": "1000000.0"
              }
            },
            {
              "currency": "PAX",
              "max_fraction": 2,
              "min_amount": {
                "currency": "PAX",
                "amount": "100.0"
              },
              "max_amount": {
                "currency": "PAX",
                "amount": "1000000.0"
              }
            }
          ],
          "basic_apr": "0.0999",
          "staked_mco_tiers": [
            {
              "apr": "0.08",
              "lockup_plans": [
                "mco500",
                "mco5000",
                "mco50000"
              ]
            }
          ]
        }
      }
    ]
  },
  "ok": true
}`
	var txes CreditAccountParse
	err := json.Unmarshal([]byte(str), &txes)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(txes.CryptoCreditAccount.OutstandingBalance)

	//quotationKey := "BUY:BTC:USD"
	//
	//coinCurrency := strings.Split(quotationKey, ":")[1]
	//fiatCurrency := strings.Split(quotationKey, ":")[2]
	//coinCurrencyUpper := strings.ToUpper(coinCurrency)
	//fiatCurrencyUpper := strings.ToUpper(fiatCurrency)
	//fmt.Println(coinCurrency)
	//fmt.Println(fiatCurrency)
	//fmt.Println(coinCurrencyUpper)
	//fmt.Println(fiatCurrencyUpper)

	fmt.Println(StringToFloat64("0"))
	fmt.Println(StringToFloat64("0.0"))

	//str := `{"account":{"id":2324,"native_currency":"USD","balance":{"currency":"USD","amount":"33575352.24"},"native_balance":{"currency":"USD","amount":"33575352.24"},"amount_change":{"currency":"USD","amount":"95483.35"},"percentage_change":"0.29","wallets":[{"currency":"MCO","balance":{"currency":"MCO","amount":"9970299.59273975"},"native_balance":{"currency":"USD","amount":"33476128.35"},"available":{"currency":"MCO","amount":"9920299.59273975"},"native_available":{"currency":"USD","amount":"33308249.1"},"locked":{"currency":"MCO","amount":"50000.0"},"native_locked":{"currency":"USD","amount":"208150.0"},"locked_until":"2020-03-15T03:18:00.000Z","percentage_change":"0.29","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"MCO","amount":"9970299.59"},"display_available":{"currency":"MCO","amount":"9920299.59"},"display_locked":{"currency":"MCO","amount":"50000"},"major_wallet":true},{"currency":"CRO","balance":{"currency":"CRO","amount":"952.07833905"},"native_balance":{"currency":"USD","amount":"57.12"},"available":{"currency":"CRO","amount":"952.07833905"},"native_available":{"currency":"USD","amount":"57.12"},"locked":{"currency":"CRO","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"-0.01","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"CRO","amount":"952.07"},"display_available":{"currency":"CRO","amount":"952.07"},"display_locked":{"currency":"CRO","amount":"0"},"major_wallet":true},{"currency":"BTC","balance":{"currency":"BTC","amount":"10.19924141"},"native_balance":{"currency":"USD","amount":"98380.09"},"available":{"currency":"BTC","amount":"10.19924141"},"native_available":{"currency":"USD","amount":"98380.09"},"locked":{"currency":"BTC","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"-1.32","address":"2NCWhMEt2zVJvHXnAuLFywaJF8womAnBYsX","display_balance":{"currency":"BTC","amount":"10.19924141"},"display_available":{"currency":"BTC","amount":"10.19924141"},"display_locked":{"currency":"BTC","amount":"0"},"major_wallet":true},{"currency":"TUSD","balance":{"currency":"TUSD","amount":"788.25706848"},"native_balance":{"currency":"USD","amount":"786.68"},"available":{"currency":"TUSD","amount":"788.25706848"},"native_available":{"currency":"USD","amount":"786.68"},"locked":{"currency":"TUSD","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"-0.09","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"TUSD","amount":"788.25"},"display_available":{"currency":"TUSD","amount":"788.25"},"display_locked":{"currency":"TUSD","amount":"0"},"major_wallet":true},{"currency":"ETH","balance":{"currency":"ETH","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"ETH","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"ETH","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"ETH","amount":"0"},"display_available":{"currency":"ETH","amount":"0"},"display_locked":{"currency":"ETH","amount":"0"},"major_wallet":false},{"currency":"XRP","balance":{"currency":"XRP","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"XRP","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"XRP","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"rEJA4cvJ4eHS3ptSdo94c828Y46ZVRj2y3?tag=502301597","display_balance":{"currency":"XRP","amount":"0"},"display_available":{"currency":"XRP","amount":"0"},"display_locked":{"currency":"XRP","amount":"0"},"major_wallet":false},{"currency":"USDT","balance":{"currency":"USDT","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"USDT","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"USDT","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"USDT","amount":"0"},"display_available":{"currency":"USDT","amount":"0"},"display_locked":{"currency":"USDT","amount":"0"},"major_wallet":false},{"currency":"BCH","balance":{"currency":"BCH","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"BCH","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"BCH","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":null,"display_balance":{"currency":"BCH","amount":"0"},"display_available":{"currency":"BCH","amount":"0"},"display_locked":{"currency":"BCH","amount":"0"},"major_wallet":false},{"currency":"LTC","balance":{"currency":"LTC","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"LTC","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"LTC","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"QWVwgBNpdqpJAMS2aav2ELGQRbVAvFQXwi","display_balance":{"currency":"LTC","amount":"0"},"display_available":{"currency":"LTC","amount":"0"},"display_locked":{"currency":"LTC","amount":"0"},"major_wallet":false},{"currency":"EOS","balance":{"currency":"EOS","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"EOS","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"EOS","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"cryptocomeos?tag=1003052058","display_balance":{"currency":"EOS","amount":"0"},"display_available":{"currency":"EOS","amount":"0"},"display_locked":{"currency":"EOS","amount":"0"},"major_wallet":false},{"currency":"BNB","balance":{"currency":"BNB","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"BNB","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"BNB","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"tbnb1qra725g9slk60t8j4236tdyxup9f0k3kysyknp?tag=2096144794","display_balance":{"currency":"BNB","amount":"0"},"display_available":{"currency":"BNB","amount":"0"},"display_locked":{"currency":"BNB","amount":"0"},"major_wallet":false},{"currency":"LINK","balance":{"currency":"LINK","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"LINK","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"LINK","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"LINK","amount":"0"},"display_available":{"currency":"LINK","amount":"0"},"display_locked":{"currency":"LINK","amount":"0"},"major_wallet":false},{"currency":"USDC","balance":{"currency":"USDC","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"USDC","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"USDC","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"USDC","amount":"0"},"display_available":{"currency":"USDC","amount":"0"},"display_locked":{"currency":"USDC","amount":"0"},"major_wallet":false},{"currency":"BAT","balance":{"currency":"BAT","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"BAT","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"BAT","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"BAT","amount":"0"},"display_available":{"currency":"BAT","amount":"0"},"display_locked":{"currency":"BAT","amount":"0"},"major_wallet":false},{"currency":"PAX","balance":{"currency":"PAX","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"PAX","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"PAX","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"PAX","amount":"0"},"display_available":{"currency":"PAX","amount":"0"},"display_locked":{"currency":"PAX","amount":"0"},"major_wallet":false},{"currency":"OMG","balance":{"currency":"OMG","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"OMG","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"OMG","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"OMG","amount":"0"},"display_available":{"currency":"OMG","amount":"0"},"display_locked":{"currency":"OMG","amount":"0"},"major_wallet":false},{"currency":"DAI","balance":{"currency":"DAI","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"DAI","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"DAI","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":"0xC879658B273A7e6d65e08F08e50b4A82B39104bC","display_balance":{"currency":"DAI","amount":"0"},"display_available":{"currency":"DAI","amount":"0"},"display_locked":{"currency":"DAI","amount":"0"},"major_wallet":false},{"currency":"ONE","balance":{"currency":"ONE","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"ONE","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"ONE","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":null,"display_balance":{"currency":"ONE","amount":"0"},"display_available":{"currency":"ONE","amount":"0"},"display_locked":{"currency":"ONE","amount":"0"},"major_wallet":false},{"currency":"USDM","balance":{"currency":"USDM","amount":"0.0"},"native_balance":{"currency":"USD","amount":"0.0"},"available":{"currency":"USDM","amount":"0.0"},"native_available":{"currency":"USD","amount":"0.0"},"locked":{"currency":"USDM","amount":"0.0"},"native_locked":{"currency":"USD","amount":"0.0"},"locked_until":null,"percentage_change":"0.0","address":null,"display_balance":{"currency":"USDM","amount":"0"},"display_available":{"currency":"USDM","amount":"0"},"display_locked":{"currency":"USDM","amount":"0"},"major_wallet":false}]},"ok":true}`
	//
	//txes := AccountParse{}
	//err := json.Unmarshal([]byte(str), &txes)
	//if err != nil {
	//	fmt.Println("Rail to unmarshal Rails response: ", err)
	//}
	//fmt.Println(txes.Account.Id)

	//students := pase_student()
	//for k, v := range students {
	//	fmt.Printf("key=%s,value=%v \n", k, v)
	//}

	//r := InitMap()
	//
	//fmt.Println(r["crypto_credit_liquidation_created"])

	//startData := "2019-08-23T07:49:11.183Z"
	//
	//timeStampStartData, err := time.Parse("2006-01-02T15:04Z", startData)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Println(timeStampStartData)



	//var data = "{\"price\":{\"10.0000000000\":\"1.36412\",},\"src\":\"cmm1\",\"symbol\":\"SGDUSD\",\"timestamp\":1574560973}"
	//
	//if strings.Contains(data, ",},") {
	//	data = strings.Replace(data, ",},", "},", -1)
	//}
	//
	//fmt.Println(data)

	//client := retryablehttp.Client{
	//	RetryWaitMin: time.Duration( 2 * time.Minute),
	//	RetryMax : 3,
	//}
	//
	//req, err := retryablehttp.NewRequest(http.MethodGet, "/foo", nil)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//
	//resp, err := client.Do(req)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//fmt.Println(err)
	//fmt.Println(resp)
}